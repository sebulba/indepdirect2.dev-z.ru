/**
 * Created by mi on 22.09.15.
 */
function sendConditions(element, index, array)
{
    $.post(
        "/larin/conditions/",
        {element},
        function(html)
        {
            console.log(html);
            quotedText = /\<companyName\>([\w]*)/;
            company = quotedText.exec(html)[1];
            currentCompany = $('#'+company)[0];
            arAdvantages.forEach(function(element, index, array){
                if(html.indexOf(element) >= 1)
                {
                    $($(currentCompany).children('.service-item')[index]).children('img').removeClass('not_available');
                }
            })
        }
    );
}
$(document).ready(function()
{
    arAdvantages = ['Круглосуточный координационный центр','Тех. помощь','Эвакуация','Сбор справок','Аварийный комиссар']
    arRequest.forEach(sendConditions);
    $('.load-more').click(function()
    {
        $('.insurance-item').show();
        $('.info-insurance-block').show();

        $(this).parent().hide();
    })
})
