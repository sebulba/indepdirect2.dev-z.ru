<?
//var_dump
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
$wsdl = COption::GetOptionString('scid.indepdirect', 'indepdirect_soap_wsdl'); //"http://local.elt-poisk.com/INDEP-DEV/soap.php?wsdl";
preg_match_all('/\[([\w\d_\.]*)\]/',$_POST['element'], $match);
//    $sendData = array(
//        "InsuranceCompany" => "RGS",
//        "ProductId" => "WEB_KASKO_AS",
//        "ProgramId" => "0210",
//        "CarCost" => "1045000.00",
//        "PremiumSum" => "54863.00",
//        "Mark" => "AUDI",
//        "Model" => "A4"
//    );

global $DB;
$strSql = 'SELECT PROPERTY_109 from b_iblock_element_prop_s4 where IBLOCK_ELEMENT_ID = '.$match[1][6].'';
$res = $DB->Query($strSql, false, $err_mess.__LINE__);
$programId= $res->Fetch();

$sendData = array(
    "InsuranceCompany" => $match[1][0],
    "ProductId" => $match[1][1],
    "ProgramId" => $programId[0],
    "CarCost" => $match[1][2],
    "PremiumSum" => $match[1][3],
    "Mark" => $match[1][4],
    "Model" => $match[1][5]
);


$start = microtime();

$soap = new SoapClient($wsdl, array('exceptions' => false, 'connection_timeout' => 10));

$result = $soap->GetConditions($sendData);

$finish = microtime();

echo '<pre>';
print_r($sendData);
echo '</pre>';

if (is_soap_fault($result)) {
    echo $result->faultstring . ' (' . $result->faultcode . ')';
}
else {
    echo '<pre>';
    print_r($result->Conditions->Condition);
    echo '</pre>';
}

$total = ($finish - $start) / 1000;

echo "Script time: ".$total." sec";
echo '<companyName>'.$match[1][0].'<companyName>';
?>