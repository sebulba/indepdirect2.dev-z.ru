<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddHeadScript('/bitrix/js/getConditions/getConditions.ajax.js');
?>
<script>
    arRequest = [];
</script>
<?if(count($arResult["ITEMS"]) > 0 && is_array($arResult["ITEMS"])):?>
    <div class="insurance-lists mt47 nav_result_<?=$arResult['NAV_RESULT']->NavNum?>" id="offersList">
    <?foreach($arResult["ITEMS"] as $k=>$arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $arInsuranceCompany = false;
        if (strlen($arItem['PROPERTIES']['INSURANCE_COMPANY']['VALUE']) > 0)
        {
            if (array_key_exists($arItem['PROPERTIES']['INSURANCE_COMPANY']['VALUE'], $arResult['INSURANCE_COMPANY']))
            {
                $arInsuranceCompany = $arResult['INSURANCE_COMPANY'][$arItem['PROPERTIES']['INSURANCE_COMPANY']['VALUE']];
            }
        }
        $kaskoPrice = 0;
        $price = 0;
        if ($arItem['PROPERTIES']['KASKO']['VALUE'] == INDEPDIRECT_TYPE_KASKO)
        {
            $price = round(FloatVal($arItem['PROPERTIES']['PREMIUM_SUM_KASKO']['VALUE']));
        }
        elseif ($arItem['PROPERTIES']['KASKO']['VALUE'] == INDEPDIRECT_TYPE_DAMAGE)
        {
            $price = round(FloatVal($arItem['PROPERTIES']['PREMIUM_SUM_DAMAGE']['VALUE']));
        }
        $kaskoPrice = $price;
        ?>
        <div class="insurance-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" <?if($k >= 5){?>style="display:none;"<?}?>>
        <div class="short-info clearfix">
            <?
            if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) > 0)
            {
                ?>
                <span class="insurance-arrows df">Далее</span>
            <?
            }
            else
            {
                ?>
                <span class="button">
				<a class="button-buy button-green buy" rel="<?=$arItem['ID']?>" href="javascript:;">Купить</a>
			</span>
            <?
            }
            ?>
            <div class="left-column df">
                <div class="logo">
    				<span>
                        <?
                        if (is_array($arInsuranceCompany))
                        {
                            if (is_array($arInsuranceCompany['PREVIEW_PICTURE']))
                            {
                                ?>
                                <img src="<?=$arInsuranceCompany['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arInsuranceCompany['NAME']?>" />
                            <?
                            }
                        }
                        ?>
    				</span>
                </div>
                <div class="service-list" id="<?=$arInsuranceCompany['CODE']?>">
                    <?
                    preg_match_all('/\s(\w*)/',$arResult['ITEMS'][0]['NAME'],$match);
                    $mark = $match[1][0];
                    $model = $match[1][1];
                    if (is_array($arInsuranceCompany))
                    {
                        $arRequestIC[] = array(
                            'InsuranceCompany' => $arInsuranceCompany['CODE'],
                            'ProductId' => 'WEB_KASKO_AS',
                            'CarCost' => $arResult['ITEMS'][0]['PROPERTIES']['COST']['VALUE'],
                            'PremiumSum' => $price,
                            'Mark' => $mark,
                            'Model' => $model,
                            'Id' => $arInsuranceCompany['ID']
                        );

                        ?>
                        <script>
                            arRequest[<?=$k?>] = "[<?=$arRequestIC[$k]['InsuranceCompany']?>],[<?=$arRequestIC[$k]['ProductId']?>],[<?=$arRequestIC[$k]['CarCost']?>],[<?=$arRequestIC[$k]['PremiumSum']?>], [<?=$arRequestIC[$k]['Mark']?>],[<?=$arRequestIC[$k]['Model']?>],[<?=$arRequestIC[$k]['Id']?>]";
                        </script>
                        <?
                        if (is_array($arInsuranceCompany['ADVANTAGES']))
                        {
                            $arInsuranceCompany['ADVANTAGES'] = array_flip($arInsuranceCompany['ADVANTAGES']);
                        }
                        else
                        {
                            $arInsuranceCompany['ADVANTAGES'] = array();
                        }

                        foreach($arResult['ADVANTAGES'] as $v)
                        {
                            $db_props = CIBlockElement::GetProperty(11, $v['ID'],  array("sort" => "asc"), Array("CODE"=>"DESCRIPTION_POP_UP"));
                            if($ar_props = $db_props->Fetch()); $v['NAME']=$ar_props['VALUE']['TEXT'];

                            ?>
                            <div class="service-item">
                                <img src="<?=$v['PREVIEW_PICTURE']['SRC']?>" alt="<?=$v['NAME']?>"<?if(!array_key_exists($v['ID'], $arInsuranceCompany['ADVANTAGES'])):?> class="not_available"<?endif;?> />
                                <div class="service-info"><?=$v['NAME']?></div>
                            </div>
                        <?
                        }
                    }
                    ?>
                    <div class="service-item hz df">
                        <?if($kaskoPrice <= 100000):?>
                            <div class="service-info">Карта IndepDirect Basic в подарок<br> Юридическая помощь, круглосуточная справка. </div>
                        <?else:?>
                            <div class="service-info"> Карта IndepDirect Comfort в подарок<br> Юридическая помощь, круглосуточная справка, эвакуация
                                автомобиля, техпомощь на дороге, аварийный комиссар, сбор справок из
                                ГИДББ </div>
                        <?endif;?>
                    </div>
                </div>
            </div>
            <div class="right-column df">
                <fieldset>
                    <div class="short-box">
                        <?
                        if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) > 0)
                        {
                            ?>
                            <label>Каско</label>
                            <span class="numerals"><strong><?=number_format($price, 0, '.', ' ')?></strong></span>
                        <?
                        }
                        ?>
                    </div>
                    <?
                    if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) == 0)
                    {
                        ?>
                        <div class="short-box"></div>
                    <?
                    }
                    ?>
                    <div class="short-box" style="width: 80px;">
                        <?
                        $osagoPrice = 0;
                        if (strlen($arItem['PROPERTIES']['OSAGO']['VALUE']) > 0)
                        {
                            $osagoPrice = $price = round(FloatVal($arItem['PROPERTIES']['PREMIUM_SUM_OSAGO']['VALUE']));
                            ?>
                            <label>Осаго</label>
                            <span class="numerals"><strong><?=number_format($price, 0, '.', ' ')?></strong></span>
                        <?
                        }
                        ?>
                    </div>
                    <?
                    if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) > 0)
                    {
                        ?>
                        <div class="short-box">
                            <label>Франшиза</label>
                            <span class="numerals"><strong><?=number_format(IntVal($arItem['PROPERTIES']['FRANCHISE_UWIN']['VALUE']), 0, '.', ' ')?> (<?=round((IntVal($arItem['PROPERTIES']['FRANCHISE_UWIN']['VALUE'])*100)/IntVal($arItem['PROPERTIES']['COST']['VALUE'])) ?>%)</strong></span>
                        </div>
                    <?
                    }
                    ?>
                    <div class="short-box">
                        <?
                        if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) > 0)
                        {
                            $price = $kaskoPrice * 0.05;
                            ?>
                            <label>Ваша выгода</label>
                            <span class="numerals"><strong><?=number_format($price, 0, '.', ' ')?></strong></span>
                        <?
                        }
                        ?>
                    </div>
                    <div class="short-box">
                        <label class="green">Итого</label>
                        <?
                        $price = $kaskoPrice * 0.95 + $osagoPrice;
                        ?>
                        <span class="numerals"><strong><?=number_format($price, 0, '.', ' ')?></strong></span>
                    </div>
                </fieldset>
            </div>
        </div>
        <?
        if (strlen($arItem['PROPERTIES']['KASKO']['VALUE']) > 0)
        {
            ?>
            <div class="detail-info" style="display: none;">
                <form action="" method="get" onsubmit="return false;">
                    <input type="hidden" name="offerId" value="<?=$arItem['ID']?>" />
                    <input type="hidden" name="nodeId" value="<?=$this->GetEditAreaId($arItem['ID']);?>" />
                    <input type="hidden" name="Loading" value="" />
                    <fieldset>
                        <div class="detail-box">
                            <label>Риск:</label>
                            <div class="radio">
                                <input disabled="disabled" type="radio" name="TypeKASKO" value="KASKO"<?=($arItem['PROPERTIES']['KASKO']['VALUE'] == INDEPDIRECT_TYPE_KASKO) ? ' checked="checked"' : ''?> />Угон и ущерб
                            </div>
                            <?/*<div class="radio">
    						<input type="radio" name="TypeKASKO" value="DAMAGE"<?=($arItem['PROPERTIES']['KASKO']['VALUE'] == INDEPDIRECT_TYPE_DAMAGE) ? ' checked="checked"' : ''?> />Только ущерб
    					</div>*/?>
                        </div>
                        <div class="detail-box">
                            <label>Стоимость ТС:</label>
                            <div class="text long">
                                <input type="text" name="Cost" value="<?=IntVal($arItem['PROPERTIES']['COST']['VALUE'])?>" /><span class="about">руб.</span>
                                <i class="advice" style="top: 5px;">
                                    <div>Максимальная сумма выплаты в случае угона или невозможности восстановления автомобиля после аварии.
                                        Уменьшая оценку ТС стоимость полиса, конечно, снизится, но и выплата страхового возмещения по такому полису будет меньше.</div>
                                </i>
                            </div>
                        </div>
                        <div class="detail-box">
                            <label>Франшиза:</label>
                            <div class="text">
                                <select name="Franchise">
                                    <?
                                    for ($i = 0; $i <= 15; $i++)
                                    {
                                        ?>
                                        <option value="<?=$i?>"<?=($i == $arItem['PROPERTIES']['FRANCHISE']['VALUE']) ? ' selected="selected"' : ''?>><?=$i?></option>
                                        <?
                                        // if($i == 0) $i = 2;
                                    }
                                    ?>
                                </select><span class="about">%</span>
                                <i class="advice" style="top: 5px;">
                                    <div>
                                        Франшиза - величина ущерба, которую страховая компания не компенсирует при наступлении страхового случая.
                                        Франшиза позволяет снизить стоимость полиса, но надо помнить, что при страховом случае часть денег придется доплатить самостоятельно.
                                    </div>
                                </i>
                            </div>
                        </div>
                        <div class="detail-box">
                            <?
                            if (is_array($arInsuranceCompany))
                            {
                                foreach($arResult['ADVANTAGES'] as $v)
                                {
                                    ?>
                                    <?
                                    if (array_key_exists($v['ID'], $arInsuranceCompany['ADVANTAGES']))
                                    {
                                        ?>
                                        <div class="checkbox">
                                            <input type="checkbox" name="var" value="0" disabled="disabled" /><?=$v['NAME']?>
                                        </div>
                                    <?
                                    }
                                    ?>
                                <?
                                }
                            }
                            ?>
                        </div>
                    </fieldset>
                </form>
                <a href="javascript:void(0);" rel="<?=$arItem['ID']?>" class="star" title="Добавить в избранное"></a>
                <a href="javascript:void(0);" rel="<?=$arItem['ID']?>" class="email" title="Отправить на e-mail"></a>
                <div class="down active"><span>Скрыть настройки</span></div><a href="javascript:void(0);" class="buy" onClick="ga('send', 'event', 'Купить предложение', 'click', 'Купить предложение');yaCounter24835679.reachGoal('buy_offers');" rel="<?=$arItem['ID']?>"><span>Купить</span></a>
            </div>
        <?
        }
        ?>
        <div class="loading"></div>
        </div>
    <?endforeach;?>
    </div>
    <div class="load-more-holder" style="display: block;">
        <div class="load-more"><span>Загрузить еще</span></div>
    </div>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>

    <script type="text/javascript">
        window['onNavResult_<?=$arResult['NAV_RESULT']->NavNum?>'] = function() {
            $('input').styler();
            offersList.updateFavorite($('#offersList'));
        };
        window['onNavResult_<?=$arResult['NAV_RESULT']->NavNum?>']();
    </script>
<?else:?>
    <p style="margin-top: 30px;">
        <a class="button button-blue" href="javascript:;" onclick="$('.header').find('span.email').trigger('click');" style="height: 42px; padding-top: 30px; text-transform: uppercase; font-weight: bold; font-size: 18px;">Связаться с нами</a>
    </p>
<?endif;?>
