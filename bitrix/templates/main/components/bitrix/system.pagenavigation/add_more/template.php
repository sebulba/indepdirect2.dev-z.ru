<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$idNav = md5(RandString(32));
?>

    <noscript>
        <div class="navigation_more">
            <?if($arResult["bDescPageNumbering"] === true):?>

                <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                    <?if($arResult["bSavePage"]):?>
                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
                    <?else:?>
                        <?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
                            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
                        <?else:?>
                            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
                        <?endif?>
                    <?endif?>
                <?endif?>

                <?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
                    <?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

                    <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <b><?=$NavRecordGroupPrint?></b>
                    <?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a>
                    <?else:?>
                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
                    <?endif?>

                    <?$arResult["nStartPage"]--?>
                <?endwhile?>

                <?if ($arResult["NavPageNomer"] > 1):?>
                    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_next")?></a>
                <?endif?>

            <?else:?>

                <?if ($arResult["NavPageNomer"] > 1):?>

                    <?if($arResult["bSavePage"]):?>
                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
                    <?else:?>
                        <?if ($arResult["NavPageNomer"] > 2):?>
                            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
                        <?else:?>
                            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
                        <?endif?>
                    <?endif?>

                <?endif?>

                <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

                    <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <b><?=$arResult["nStartPage"]?></b>
                    <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
                    <?else:?>
                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
                    <?endif?>
                    <?$arResult["nStartPage"]++?>
                <?endwhile?>

                <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_next")?></a>
                <?endif?>

            <?endif?>
        </div>
    </noscript>

<?
if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
    ?>
    <div class="load-more-holder" id="nav_<?=$idNav?>">
        <div class="load-more"><span>Загрузить еще</span></div>
    </div>
    <script type="text/javascript">
        var nav_<?=$idNav?> = {
            this_page : <?=$arResult["NavPageNomer"]?>,
            max_page : <?=$arResult["NavPageCount"]?>
        };
        $('#nav_<?=$idNav?>').show().find('.load-more').click(function(e){
            e.preventDefault();
            if ($(this).hasClass('loading'))
            {
                return;
            }
            $(this).addClass('loading');
            var content = $('.nav_result_<?=$arResult["NavNum"]?>');
            if (content.length > 0) {
                if (nav_<?=$idNav?>.this_page < nav_<?=$idNav?>.max_page) {
                    nav_<?=$idNav?>.this_page++;
                    $.ajax({
                        dataType : 'html',
                        cache : false,
                        data : {'PAGEN_<?=$arResult["NavNum"]?>' : nav_<?=$idNav?>.this_page, 'AJAX_<?=$arResult["NavNum"]?>' : 'Y'},
                        success : function(r){
                            content.append($(r).find('.nav_result_<?=$arResult["NavNum"]?>').contents());
                            if (nav_<?=$idNav?>.this_page >= nav_<?=$idNav?>.max_page) {
                                $('#nav_<?=$idNav?>').remove();
                            }
                            if (typeof window['onNavResult_<?=$arResult["NavNum"]?>'] != 'undefined')
                            {
                                window['onNavResult_<?=$arResult["NavNum"]?>']();
                            }
                        }
                    });
                }
            }
        });
    </script>
<?
}
?>